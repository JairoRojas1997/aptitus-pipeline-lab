.DEFAULT_GOAL := help

## GENERAL ##
OWNER               = aptitus
SERVICE_NAME_TEST   = testrestfull-test

## DEPLOY ##
ENV 			?= dev
export DEPLOY_REGION 	?= eu-west-1
ACCOUNT_ID		?= 929226109038
SERVER_NAME     ?= dkr.ecr.eu-west-1.amazonaws.com

## RESULT_VARS ##
PROJECT_NAME_TEST = $(OWNER)-$(ENV)-$(SERVICE_NAME_TEST)
export IMAGE_TEST = $(ACCOUNT_ID).$(SERVER_NAME)/$(PROJECT_NAME_TEST):20180622.33

## Target Commons ##

install: ## Install project
	./script.sh install --env=${env} --cdn=${cdn}

build: ## build image: make build, make build image=nginx
	./script.sh build ${image}

up: ## Up docker containers: make up
	./script.sh start

down: ## Stops and removes the docker containers: make down
	./script.sh stop

restart: ## Restart all containers: make restart
	docker-compose restart

status: ## Show containers status: make status
	docker-compose ps

log: ## Show container logs: make log image=nginx
	docker-compose logs -f $(image)

composer: ## Install composer dependency: make composer req=symfony/dotenv
	./script.sh composer require ${req} --no-progress --profile --prefer-dist

composer-update: ## Update composer dependencies
	./script.sh composer update --no-progress --profile --prefer-dist

tests-e2e: ## Run the end to end Tests: make tests-e2e
	docker-compose -f docker-compose.test.yml run --rm test

ssh: ## Enter ssh container: make ssh container=nginx
	docker run -ti ${container} sh

push: ## Push all images to registry
	./script.sh push

pull: ## Pull all images from registry
	./script.sh pull

cache: ## Pull all images from registry
	./script.sh composer cache:clear

frontend-build: ## Run build frontend task
	./script.sh yarn build

gulp: ## Run gulp task
	./script.sh yarn gulp ${run}

clean: ## Clear containers
	docker container prune -f ; \
	docker image prune -f ; \
	docker network prune -f ; \
	docker volume prune -f;

## Target Help ##

help:
	@printf "\033[31m%-16s %-59s %s\033[0m\n" "Target" "Help" "Usage"; \
	printf "\033[31m%-16s %-59s %s\033[0m\n" "------" "----" "-----"; \
	grep -hE '^\S+:.*## .*$$' $(MAKEFILE_LIST) | sed -e 's/:.*##\s*/:/' | sort | awk 'BEGIN {FS = ":"}; {printf "\033[32m%-16s\033[0m %-58s \033[34m%s\033[0m\n", $$1, $$2, $$3}'

#!/usr/bin/env groovy

def fnSteps = evaluate readTrusted("deploy/scripts/steps.groovy")
def fnSlack = evaluate readTrusted("deploy/scripts/slack.groovy")

pipeline {
  agent any
  options {
    disableConcurrentBuilds()
  }
  environment {
    DEVELOPMENT_ENV = 'dev'
    STAGING_ENV = 'pre'
    PRODUCTION_ENV = 'prod'
  }
  parameters {
    booleanParam(name: 'DEVELOPMENT', defaultValue: true, description: "Ejecutar Development")
    booleanParam(name: 'STAGING', defaultValue: false, description: "Ejecutar Staging")
    booleanParam(name: 'PRODUCTION', defaultValue: false, description: "Ejecutar Production")
    choice(
      name: 'EXECUTE',
      choices:"DEPLOY\nROLLBACK\nREGISTRY",
      description: '''DEPLOY: Se realiza deploy del servicio
ROLLBACK: Rollback de la última migración
REGISTRY: Requiere construir o no Registry en ECR'''
    )
  }
  stages {
    stage('Checkout') {
      steps {
        script {
         //wrap([$class: 'BuildUser']) {
         // BUILD_USER_ID = "${BUILD_USER_ID}"
         //}
        }
        checkout scm
        // sh 'git submodule update --init --recursive'
      }
    }
    stage('Development') {
      when {
        expression {
          return params.DEVELOPMENT
        }
      }
      stages {
        stage('Config') {
          steps {
            script {
              config = fnSteps.configs(DEVELOPMENT_ENV)
              fnSlack.start(config)
            }
          }
        }
        stage ('Job') {
          stages {
            stage('Deploy') {
              when { expression { return params.EXECUTE == 'DEPLOY' }}
              steps {
                script {
                  fnSteps.deploy(config)
                }
              }
            }
            stage('Rollback') {
              when { expression { return params.EXECUTE == 'ROLLBACK' }}
              steps { script { fnSteps.rollback(config) } }
            }
            stage('ECR') {
              when { expression { return params.EXECUTE == 'REGISTRY' }}
              steps { script { fnSteps.registry(config) } }
            }
          }
        }
      }

      post {
        success {
          script { fnSlack.success(config) }
        }
      }
    }

    stage ('Staging') {
      when {
        expression {
          return params.STAGING
        }
      }
      stages {
        stage('Config') {
          steps {
            script {
              config = fnSteps.configs(STAGING_ENV)
              userFound = config.ALLOWED_USERS.split(',').find { item -> item == BUILD_USER_ID }
            }
          }
        }
        stage('Validate') {
          when {
            expression { return userFound == null }
          }
          steps {
            script {
              input (message: "Continue deployment to Staging?", submitter: "${config.ALLOWED_USERS}")
              fnSlack.start(config)
            }
          }
        }
        stage('Job') {
          stages {
            stage('Deploy') {
              when { expression { return params.EXECUTE == 'DEPLOY' }}
              steps {
                script {
                  fnSteps.deploy(config)
                  fnSteps.testsIntegration(config)
                }
              }
            }
            stage('Rollback') {
              when { expression { return params.EXECUTE == 'ROLLBACK' }}
              steps { script { fnSteps.rollback(config) } }
            }
            stage('ECR') {
              when { expression { return params.EXECUTE == 'REGISTRY' }}
              steps { script { fnSteps.registry(config) } }
            }
          }
        }
      }
      post {
        success {
          script { fnSlack.success(config) }
        }
      }
    }

    stage ('Production') {
      when {
        expression {
          return params.PRODUCTION
        }
      }
      stages {
        stage ('Config') {
          steps {
            script {
              config = fnSteps.configs(PRODUCTION_ENV)
              userFound = config.ALLOWED_USERS.split(',').find { item -> item == BUILD_USER_ID }
            }
          }
        }
        stage ('Validate') {
          when {
            expression { return userFound == null }
          }
          steps {
            script {
              input (message: "Continue deployment to Production?", submitter: "${config.ALLOWED_USERS}")
              fnSlack.start(config)
            }
          }
        }
        stage('Production') {
          stages {
            stage('Deploy') {
              when { expression { return params.EXECUTE == 'DEPLOY' }}
              steps { script { fnSteps.deploy(config) } }
            }
            stage('Rollback') {
              when { expression { return params.EXECUTE == 'ROLLBACK' }}
              steps { script { fnSteps.rollback(config) } }
            }
            stage('ECR') {
              when { expression { return params.EXECUTE == 'REGISTRY' }}
              steps { script { fnSteps.registry(config) } }
            }
          }
        }
      }
      post {
        success {
          script { fnSlack.success(config) }
        }
      }
    }
  }
  post {
    always {
      junit '**/build/reports/xunit/xml/*.xml'
    }
    failure {
      script { fnSlack.failure(config) }
    }
    aborted {
      script { fnSlack.abort(config) }
    }
  }
}

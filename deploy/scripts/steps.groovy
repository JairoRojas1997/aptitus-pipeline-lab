def deploy(def config) {
  withEnv(config.withEnv) {
    sh 'make install'
    sh 'make build-latest'
    sh 'make test-project'
    sh 'make migrate'
    sh 'make publish'
    sh 'make update-service'
  }
}

def rollback(def config) {
  withEnv(config.withEnv) {
    sh 'make install'
    sh 'make rollback'
  }
}

def registry(def config) {
  withEnv(config.withEnv) {
    sh 'make create-registry'
  }
}

def testsIntegration(def config) {
   withEnv(config.withEnv) {
     sh 'make test-pull-image'
     sh 'make test-integration'
   }
}

def configs(def enviroment) {
  def envConfig = [
    "ENV=${enviroment}",
    "INFRA_BUCKET=infraestructura.${enviroment}"
  ]

  withEnv(envConfig) {
    sh "make sync-config-deploy"
    sh "make sync-config"
  }

  configFile = readYaml file: 'deploy/jenkins.private.yml'
  config = configFile.params

  config.withEnv = [
    "ENV=${config.ENV}",
    "DEPLOY_REGION=${config.DEPLOY_REGION}",
    "DESIRED_COUNT=${config.DESIRED_COUNT}",
    "MIN_SCALING=${config.MIN_SCALING}",
    "MAX_SCALING=${config.MAX_SCALING}",
    "HTTPS_PRIORITY=${config.HTTPS_PRIORITY}",
    "MEMORY_SIZE=${config.MEMORY_SIZE}",
    "INFRA_BUCKET=${config.INFRA_BUCKET}"
  ]

  return config
}

return this

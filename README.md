# Feria ExpoGrados

#### Install project
```
make install
```
#### Run back containers
```
make up
```
#### frontend build
```
make frontend-build
```
#### Compile all yarn tasks
```
make gulp
```
#### Compile an specific yarn task
`Tasks: clean, gzip, html, svgicons`
```
make gulp run=html
```
#### Manual tasks for Front-ends
```
cd app/frontend
rm -rf node_modules
yarn install
yarn build
```

Check `package.json` for other tasks

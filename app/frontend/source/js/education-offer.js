import './common';
import './../css/education-offer.scss';
import ContactEducation from './education-offer/contact_education';
import FormScrollContact from './education-offer/form_scroll_contact';
import ShareSocialNetworks from './education-offer/share_social_networks';
import AccordionTitle from './education-offer/accordion_title';
import ContainerFloat from './education-offer/container_float';
import ViewMoreInformation from './education-offer/view_more_information';

window.addEventListener('load', () => {
  new ContactEducation();
  new ShareSocialNetworks();
  new FormScrollContact();
  new AccordionTitle();
  new ContainerFloat();
  new ViewMoreInformation();
});

/**
 * Modulo para cargar el mapa de google
 * @class LoadMap
 * @main Stand
 * @author Christiam Mendives
 */

/*global $*/
/*global _*/
/*global google*/
import _ from "underscore";

export default class LoadMap {
  constructor () {
    this.setSettings();
    this.catchDom();
    this.afterCatchDom();
    this.subscribeEvents();
  }

  setSettings() {
    this.st = {
      contentMap          : '.js-content',
      mapLocation         : '.js-map-location',
      locationButton      : '.js-map-click',
      map 				        : '#mapCanvas',
      templateStaticImage : '#staticImage',
      imageMap 			      : '#imageMap'
    };
    this.mapScript = {
      type: 'text/javascript',
      src : 'https://maps.googleapis.com/maps/api/js?callback=initializeMap'
    };
    this.dom = {};
    this.global = {
      latitude : 0,
      longitude: 0,
      sizeWidth: 640
    }
  }

  catchDom() {
    this.dom.contentMap          = $(this.st.contentMap);
    this.dom.mapLocation         = $(this.st.mapLocation, this.dom.contentMap);
    this.dom.locationButton      = $(this.st.locationButton, this.dom.mapLocation);
    this.dom.map                 = $(this.st.map, this.st.mapLocation);
    this.dom.templateStaticImage = $(this.st.templateStaticImage);

    this.global.latitude         = this.dom.map.attr('data-latitude');
    this.global.longitude        = this.dom.map.attr('data-longitude');
  }

  afterCatchDom() {
    if(this.dom.mapLocation.length) this.fnLoadImageMap();
  }

  subscribeEvents() {
    this.dom.locationButton.on('click', () => this.fnMainloadMap());
  }

  fnMainloadMap() {
    this.fnLoadScript();
    if( this.dom.imageMap !== undefined ) {
      this.dom.imageMap.hide();
    }
    this.dom.imageMap.show();
  }

  fnLoadImageMap() {
    this.fnShowMapImage(this.dom.map, this.dom.templateStaticImage, this.global.latitude, this.global.longitude )
    this.dom.imageMap = $(this.st.imageMap, this.dom.mapLocation);
  }

  fnShowMapImage(mapLocation, templateStaticImage, latitude, longitude) {
    let template = _.template(templateStaticImage.html());
    let data = {
      sizeWidth : this.global.sizeWidth,
      latitud   : latitude,
      longitud  : longitude,
    };
    mapLocation.append(template(data))
  }

  fnLoadScript (){
    window.initializeMap = () => {
      this.fnInitializeMap(this.global.latitude, this.global.longitude);
    };

    let script = document.createElement('script');
    script.type = this.mapScript.type;
    script.src  = this.mapScript.src;

    document.body.appendChild(script);
  }

  fnInitializeMap(latitude, longitude) {
    let myLatlng = new google.maps.LatLng(latitude, longitude);
    let mapOptions = {
      scrollwheel: false,
      zoom 		   : 17,
      minZoom		 : 5,
      center 		 : myLatlng
    };
    let map = this.fnGenerateMap(this.dom.map[0], mapOptions);
    let marker = this.fnGenerateMarker(myLatlng);
    marker.setMap(map);
  }

  fnGenerateMap(divmap, mapOptions) {
    let map = new google.maps.Map(divmap, mapOptions);
    return map;
  }
  fnGenerateMarker(myLatlng) {
    let marker = new google.maps.Marker({
      position: myLatlng,
      title: ''
    });
    return marker;
  }
}

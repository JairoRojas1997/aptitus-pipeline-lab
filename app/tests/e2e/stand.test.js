const request = require('@request');
const mysql = require('@mysql');

describe('Stand Services', () => {

    test('Carga de datos del stand', async () => {
        let category = 'Educacion';
        let stand = await mysql(`SELECT * FROM company_fair cf INNER JOIN company c ON c.id = cf.company_id WHERE cf.category="${category}" AND cf.state=1 AND c.state=1 ORDER BY cf.id DESC LIMIT 1`);

        let { body, statusCode } = await request(`/educacion/companies/${stand[0].slug}/stand`);
        expect(statusCode).toEqual(200);
    });

    test('Carga de ofertas de la institucion', async () => {
        let category = 'Educacion';
        let stand = await mysql(`SELECT * FROM company_fair cf INNER JOIN company c ON c.id = cf.company_id WHERE cf.category="${category}" AND cf.state=1 AND c.state=1 ORDER BY cf.id DESC LIMIT 1`);

        let { body, statusCode } = await request(`/educacion/companies/${stand[0].slug}/offers`);
        expect(statusCode).toEqual(200);
    });

    test('Carga de ofertas de la institucion con busqueda', async () => {
        let category = 'Educacion';
        let stand = await mysql(`SELECT * FROM company_fair cf INNER JOIN company c ON c.id = cf.company_id WHERE cf.category="${category}" AND cf.state=1 AND c.state=1 ORDER BY cf.id DESC LIMIT 1,1`);

        let { body, statusCode } = await request(`/educacion/companies/${stand[0].slug}/offers/empresa`);
        expect(statusCode).toEqual(200);
    });
});

const request = require('@request');
const mysql = require('@mysql');

describe('Load Page', () => {

    test('leer la base de datos', async () => {
        let results = await mysql('SELECT 4 + 2 AS solution');
        expect(results[0].solution).toEqual(6);
    });

    test('carga del documento', async () => {
        let { body, statusCode } = await request('/ajax/grades');
        expect(statusCode).toEqual(200);
    });

});

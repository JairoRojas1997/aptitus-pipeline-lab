const request = require('@request');
const mysql = require('@mysql');

describe('Load Ajax Services', () => {

    test('Carga de empresas participantes', async () => {
        let { body, statusCode } = await request('/');
        expect(statusCode).toEqual(200);
    });

    test('Carga de la Pagina Home Error', async () => {
        let { body, statusCode } = await request('/xcvxcvxcv');
        expect(statusCode).toEqual(404);
    });

});

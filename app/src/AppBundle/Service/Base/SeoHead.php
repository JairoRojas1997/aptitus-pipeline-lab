<?php

namespace AppBundle\Service\Base;

/**
 * Class SeoHead
 *
 * @package AppBundle\Service\Base
 * @author Joseph Marcilla <jbmarflo@gmail.com>
 * @copyright (c) 2017, Orbis
 */
class SeoHead
{
    public function getTitleSectionList($name)
    {
        return [
            'avisos'    => 'Ofertas educativas de ' . $name,
            'perfil'    => $name . ' - Perfil Institucional',
            'galeria'   => $name . ' - Galería',
            'documentos'=> $name . ' - Documentos Descargables'
        ];
    }

    public function getDescriptionSectionList($name, $fairCategory = null)
    {
        return [
            'avisos' => sprintf(
                'Todas las ofertas educativas de la institución %s en un solo lugar. ' .
                'Con Expogrados encuentra la carrera hacia tu futuro | Expogrados',
                $name
            ),
            'perfil' => '',
            'galeria' => '',
            'documentos' => ''
        ];
    }

    public function getDescriptionFb($name)
    {
        return sprintf("Acabo de visitar el stand virtual de %s y estoy revisando sus ofertas educativas. ".
            "Te invito a visitarlo!",
            $name
        );
    }

    public function getTitleFb()
    {
        return 'Estoy en la primera feria virtual educativa ExpoGrados 2018';
    }

    public function getDescriptionTw($name)
    {
        return sprintf("Estoy en #ExpoGrados2018 revisando las ofertas educativas del stand virtual de %s",
            $name
        );
    }
}
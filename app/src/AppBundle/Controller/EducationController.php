<?php

namespace AppBundle\Controller;

use AppBundle\Service\Base\Enum\FairCategory;
use AppBundle\Service\Base\GoogleRecaptcha;
use AppBundle\Service\Base\SeoHead;
use AppBundle\Service\Education\ContactLead;
use AppBundle\Service\Education\EducationService;
use AppBundle\Service\Fair\FairService;
use AppBundle\Service\Fair\StandService;
use AppBundle\Service\PaginationService;
use Aptitus\Common\Util\Strings;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class EducationController
 *
 *
 * @package AppBundle\Controller
 * @author Joseph Marcilla <jbmarflo@gmail.com>
 * @copyright (c) 2017, Orbis
 */
class EducationController extends Controller
{
    const FAIR_ID = 1;
    const LIMIT_RECORDS = 12;
    const NRO_PAGE_INIT = 1;

    /**
     * Lista de empresas (aptitus web) o instituciones (mas educacion)
     *
     * @Route("/educacion/companies")
     * @return Response
     */
    public function getListCompaniesService()
    {
        return $this->get(FairService::class)->listCompanies(
            self::FAIR_ID,
            FairCategory::EDUCATION_CATEGORY
        );
    }

    /**
     * Lista de avisos de institucion educativa
     *
     * @Route("/educacion/{slug}/avisos", name="education_slug_record")
     * @param Request $request
     * @param String $slug
     * @return Response
     */
    public function educationSlugAction(Request $request, $slug)
    {
        return $this->getDataViewStand($request, $slug, 'avisos');
    }

    /**
     * @Route("/educacion/{slug}/perfil-institucional", name="education_company_profile")
     * @Method("GET")
     * @param Request $request
     * @param $slug
     * @return Response
     */
    public function institutionalProfileAction(Request $request, $slug)
    {
        return $this->getDataViewStand($request, $slug, 'perfil');
    }

    /**
     * @Route("/educacion/{slug}/galeria", name="education_company_gallery")
     * @Method("GET")
     * @param Request $request
     * @param $slug
     * @return Response
     */
    public function galleryAction(Request $request, $slug)
    {
        return $this->getDataViewStand($request, $slug, 'galeria');
    }

    /**
     * @Route("/educacion/{slug}/documentos-descargables", name="education_company_documents")
     * @Method("GET")
     * @param Request $request
     * @param $slug
     * @return Response
     */
    public function documentsAction(Request $request, $slug)
    {
        return $this->getDataViewStand($request, $slug, 'documentos');
    }

    /**
     * Detalle de un aviso
     *
     * @Route("/educacion/{slug}/avisos/{slugEducation}", name="education_detail")
     * @param $slug
     * @param $slugEducation
     * @return Response
     */
    public function detailJobAction($slug, $slugEducation)
    {
        $educationDetail = $this->get(EducationService::class)->getBySlug(
            $slugEducation
        );
        $description = $this->generateDescriptionDetailJob($educationDetail['data']);

        $data = [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'data' => $educationDetail['data'],
            'url' => [
                'stand' => $this->generateUrl('education_slug_record', ['slug' => $slug]),
                'previous' => sprintf('%s-%s', $educationDetail['previous']['slug_ficha_educativa'], $educationDetail['previous']['id']),
                'next' => sprintf('%s-%s', $educationDetail['next']['slug_ficha_educativa'], $educationDetail['next']['id'])
            ],
            'slug' => $slug,
            'page_title'   => $educationDetail['data']['title'],
            'page_description' => $description,
            'page_fb_description' => $description,
            'page_fb_title' => $educationDetail['data']['title'],
            'page_tw_description' => $description,
            'showCaptcha' => GoogleRecaptcha::useCaptcha(EducationService::TIME_SHOW_CAPTCHA),
            'contactLead' => ContactLead::get()
        ];
        return $this->render('fair/education-offer.html.twig', $data);
    }

    private function getDataViewStand(Request $request, $slug, $section)
    {
        $page  = $request->query->get('page', self::NRO_PAGE_INIT);
        $limit = $request->query->get('total', self::LIMIT_RECORDS);
        $query = $request->query->get('q');
        $uri = $request->getUri();

        $stand = $this->getInfoStandService($slug);
        $data = $this->getEducationOffersList($slug, $query, $limit, $page);
        $paginator = new PaginationService(
            $data['total'],
            $page,
            $limit,
            10,
            $uri
        );

        $seoHead = new SeoHead();
        $titles = $seoHead->getTitleSectionList($stand['company']['trade_name']);
        $descriptions = $seoHead->getDescriptionSectionList($stand['company']['trade_name'], 'opciones de estudios');

        $educationList = [
            'uri'        => $uri,
            'stand'      => $stand['configuration'],
            'company'    => $stand['company'],
            'records'    => $data,
            'query'      => ucfirst(Strings::sanitizeSearch($query)),
            'paginate'   => $paginator->paginate(),
            'page_title' => $titles[$section],
            'page_description' => $descriptions[$section],
            'titles'     => $titles,
            'page_fb_description' => $seoHead->getDescriptionFb($stand['company']['trade_name']),
            'page_fb_title' => $seoHead->getTitleFb(),
            'page_tw_description' => $seoHead->getDescriptionTw($stand['company']['trade_name']),
            'actual_page' => $page
        ];

        return $this->render('fair/stand.html.twig', $educationList);
    }

    /**
     * Informacion de las ofertas de la institucion
     *
     * @Route("/educacion/companies/{slug}/offers/{query}", defaults={"query" = null})
     * @param $slug
     * @param $query
     * @return Response
     */
    public function getEducationOffersList($slug, $query = null, $limit = self::LIMIT_RECORDS, $page = self::NRO_PAGE_INIT)
    {
        return $this->get(EducationService::class)->getAll([
            'company' => $slug,
            'q'       => $query,
            'limit'   => $limit,
            'page'    => $page
        ]);
    }

    /**
     * Informacion stand de la institucion
     *
     * @Route("/educacion/companies/{slug}/stand")
     * @param $slug
     * @return Response
     */
    public function getInfoStandService($slug)
    {
        return $this->get(StandService::class)->getInfoStand(FairService::FAIR_ID, $slug, FairCategory::EDUCATION_CATEGORY);
    }

    /**
     * @Route("/ajax/education/lead", name="ajax_education_lead")
     * @Method("POST")
     * @param Request $request
     * @return JsonResponse
     */
    public function setContactLead(Request $request)
    {
        $name = $request->request->get('name', null);
        $email = $request->request->get('email', null);
        $phone = $request->request->get('phone', null);
        $message = $request->request->get('message', null);
        $captchaResponse =  $request->request->get('g-recaptcha-response', null);
        $id = $request->request->get('id', null);
        $lead = $this->get(EducationService::class)->setLead(
            $id,
            [
                'name' => $name,
                'email' => $email,
                'phone' => $phone,
                'message' => $message
            ],
            $captchaResponse
        );

        return new JsonResponse(
            [
                'message' => 'Se envió correctamente',
                'data' => $lead
            ]
        );
    }

    /**
     * @param array $data
     * @return string
     */
    private function generateDescriptionDetailJob($data)
    {
        return sprintf("Bienvenido a expogrados. %s ofrece un %s a un super precio. ".
            "Con Expogrados encuentra la carrera hacia tu futuro | ExpoGrados",
            $data['institution_name'],
            $data['title']
        );
    }
}
